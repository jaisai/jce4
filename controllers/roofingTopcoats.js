var express = require('express');
var api = express.Router();



// see app.js for the root request this controller handles

// GET to this controller root URI
api.get("/", function (request, response) {
  return response.render('roofing_topcoats/index.ejs');
});
api.get('/findall', function(req, res){
    res.setHeader('Content-Type', 'application/json');
    var data = req.app.locals.roofingTopcoats.query;
    res.send(JSON.stringify(data));
});




module.exports = api;